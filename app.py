from flask_cors import CORS
from flask import jsonify, request
import conf.read_config as conf_reader
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import gencode_adapter.db_request
import gencode_adapter.tools.parse_results
import gencode_adapter.preprocess.process_data

DEBUG = True
SERVICE="gencode"
VERSION="v1"

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app, resources={r'/*': {'origins': '*'}})
SWAGGER_URL = '/genocde/v1/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "GENCODE Adapter"
    },
)

# definitions
SITE = {
    'version': '0.0.1'
}


@app.route(f'/{SERVICE}/{VERSION}/hg38/GenomicFunctionalRegions', methods=['GET'])
def get_functional_elements():
    if request.args.get("response_type") is not None:
        return_dc = True
    else:
        return_dc = False
    genomic_locations = request.args.get("genompos").split(",")

    results = gencode_adapter.db_request.get_functional_element_of_genomic_position(genomic_locations)
    results = gencode_adapter.tools.parse_results.generate_sections(results)
    results = gencode_adapter.tools.parse_results.generate_key_value_pairs_for_attributes(results)
    results = gencode_adapter.preprocess.process_data.generate_transcript_dictionary(results)
    # assign exons to transcripts
    results = gencode_adapter.preprocess.assign_exons_to_transcripts(results)
    # assign CDS to transcripts
    results = gencode_adapter.preprocess.assign_cds_to_transcripts(results)
    results = gencode_adapter.preprocess.assign_utr_to_transcripts(results)
    results = gencode_adapter.preprocess.identify_mane_select_transcript(results)
    #print("get protein sequences")
    results = gencode_adapter.preprocess.get_protein_sequences_full(results)
    #print("get pdb ids")
    results = gencode_adapter.preprocess.get_pdb_mappings(results)

    # Format transcripts
    for var in results.keys():
        transcripts_str = ""
        if "transcripts" in results[var].keys():
            for transcript in results[var]["transcripts"]:
                transcripts_str += transcript + ","
        transcripts_str = transcripts_str.rstrip(",")
        results[var]["transcript_list"] = transcripts_str

    return results


@app.route(f'/{SERVICE}/{VERSION}/hg38/GeneData', methods=['GET'])
def get_gene_data():
    if request.args.get("response_type") is not None:
        return_dc = True
    else:
        return_dc = False

    genes = request.args.get("genes").split(",")

    results = gencode_adapter.db_request.gene_request(genes, return_dc)
    results = gencode_adapter.tools.parse_results.generate_sections(results)
    results = gencode_adapter.tools.parse_results.generate_key_value_pairs_for_attributes(results)

    return results

@app.route(f'/{SERVICE}/{VERSION}/hg38/GeneFunctionalElements', methods=['GET'])
def get_gene_functional_elements():
    if request.args.get("response_type") is not None:
        return_dc = True
    else:
        return_dc = False

    genes = request.args.get("genes").split(",")
    print("gene request")

    results = gencode_adapter.db_request.gene_request(genes, return_dc)
    results = gencode_adapter.tools.parse_results.generate_sections(results)
    results = gencode_adapter.tools.parse_results.generate_key_value_pairs_for_attributes(results)

    results = gencode_adapter.preprocess.process_data.generate_transcript_dictionary(results)
    # assign exons to transcripts
    results = gencode_adapter.preprocess.assign_exons_to_transcripts(results)
    # assign CDS to transcripts
    results = gencode_adapter.preprocess.assign_cds_to_transcripts(results)
    results = gencode_adapter.preprocess.assign_utr_to_transcripts(results)
    results = gencode_adapter.preprocess.identify_mane_select_transcript(results)

    #print("Transcripts ")
    results = gencode_adapter.preprocess.get_protein_sequences_full(results)
    #results = gencode_adapter.preprocess.get_pdb_mappings(results)

    # get RefSeq mappings of Ensembl transcripts
    for var in results.keys():
        if "MANE_Select_transcript" in results[var]:
            if results[var]["MANE_Select_transcript"] != "":
                enst_ids = [results[var]["MANE_Select_transcript"]]
                refseq_ids = gencode_adapter.db_request.get_refseq_mapping(enst_ids)
                results[var]["MANE_Select_Refseq_mappings"] = refseq_ids[results[var]["MANE_Select_transcript"]]

    return results


@app.route(f'/{SERVICE}/{VERSION}/hg38/GenePositions', methods=['GET'])
def get_gene_positions():
    if request.args.get("response_type") is not None:
        return_dc = True
    else:
        return_dc = False

    genes = request.args.get("genes").split(",")
    gene_data = gencode_adapter.db_request.gene_position_request(genes, return_dc)
    #gene_data = gencode_adapter.tools.parse_results.generate_key_value_pairs_for_attributes(gene_data)
    return gene_data


@app.route(f'/{SERVICE}/{VERSION}/hg38/getMANESelectTranscript', methods=['GET'])
def get_transcripts():
    if request.args.get("genompos") is not None:
        genomic_locations = request.args.get("genompos").split(",")
    else:
        return {}
    results = gencode_adapter.db_request.get_mane_select_transcript(genomic_locations)
    #results = gencode_adapter.tools.parse_results.generate_key_value_pairs_for_attributes_no_sections(results)
    results = gencode_adapter.preprocess.process_data.get_protein_sequences(results)
    return results

if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=conf_reader.__GENCODE_ADAPTER_PORT__)
