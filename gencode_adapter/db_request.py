import copy
import pymysql
import conf.read_config as conf_reader
import concurrent.futures
import gencode_adapter.tools
from gencode_adapter.tools.parse_results import generate_sections

user="root"
pw="gencode-onkopus"
db_name="gencode"
db_table="gencode_data"


def get_pdb_ids(enst_ids):
    """
    Returns the associated PDB IDs for a list of Ensembl transcript IDs

    :param enst_ids:
    :return:
    """
    if len(enst_ids) == 0:
        return {}

    q = "SELECT * FROM "+ conf_reader.__GENCODE_PDB_ID_TABLE__ + " where "
    for enst_id in enst_ids:
        q += "(transcript_id='" + enst_id + "')"
        q += " OR "
    q = q.rstrip(" OR ")
    q = q + ';'
    #print(q)

    cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
    cursor = cnx.cursor()

    #mysql_q = ("select * from " + conf_reader.__GENCODE_PDB_ID_TABLE__ + " where " +
    #               "(transcript_id ='" + enst_id + "');"
    #               )
    mysql_q = q
    print(mysql_q)
    cursor.execute(mysql_q)

    res_data = {}
    result = cursor.fetchall()
    column_names = [desc[0] for desc in cursor.description]
    for ((result)) in enumerate(result):
            json_obj = {}
            for j, feature in enumerate(column_names):
                json_obj[feature] = result[1][j]

            functional_regions = copy.deepcopy(json_obj)
            enst_id = result[1][0]

            if enst_id not in res_data:
                res_data[enst_id] = []
            pdb_id = result[1][1]

            res_data[enst_id].append(pdb_id)

    cursor.close()
    cnx.close()

    return res_data


def get_refseq_mapping(enst_ids):
    """

    :param enst_ids:
    :return:
    """
    if len(enst_ids) == 0:
        return {}

    q = "SELECT * FROM "+ conf_reader.__GENCODE_REFSEQ_MAPPING_TABLE__ + " where "
    for enst_id in enst_ids:
        q += "(ensembl_transcript='" + enst_id + "')"
        q += " OR "
    q = q.rstrip(" OR ")
    q = q + ';'

    cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
    cursor = cnx.cursor()
    #mysql_q = ("select * from " + conf_reader.__GENCODE_REFSEQ_MAPPING_TABLE__ + " where " +
    #               "(ensembl_transcript ='" + enst_id + "');"
    #               )
    mysql_q = q
    print(mysql_q)
    cursor.execute(mysql_q)

    res_data={}
    result = cursor.fetchall()
    column_names = [desc[0] for desc in cursor.description]
    for ((result)) in enumerate(result):
        json_obj = {}
        for j, feature in enumerate(column_names):
            json_obj[feature] = result[1][j]

        enst_id = result[1][1]
        functional_regions = copy.deepcopy(json_obj)
        if enst_id not in res_data:
            res_data[enst_id] = []
        res_data[enst_id].append(functional_regions)

    cursor.close()
    cnx.close()

    return res_data


def get_protein_sequence(enst_ids):
    """
    Retrieves protein sequences for a list of Ensembl transcript IDs

    :param enst_ids:
    :return:
    """
    if len(enst_ids) == 0:
        return {}

    q = "SELECT * FROM "+ conf_reader.__GENCODE_PROTEIN_SEQUENCE_TABLE__ + " where "
    for enst_id in enst_ids:
        q += "(transcript_id='" + enst_id + "')"
        q += " OR "
    q = q.rstrip(" OR ")
    q = q + ';'

    cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
    cursor = cnx.cursor()

    #mysql_q = ("select * from " + conf_reader.__GENCODE_PROTEIN_SEQUENCE_TABLE__ + " where " +
    #               "(transcript_id ='" + enst_id + "');"
    #               )
    mysql_q=q
    print(mysql_q)
    cursor.execute(mysql_q)

    res_data= {}
    result = cursor.fetchall()

    column_names = [desc[0] for desc in cursor.description]
    for ((result)) in enumerate(result):
        json_obj = {}
        for j, feature in enumerate(column_names):
            json_obj[feature] = result[1][j]

        functional_regions = copy.deepcopy(json_obj)
        enst_id = result[1][0]
        if enst_id not in res_data:
            res_data[enst_id] = []
        res_data[enst_id].append(functional_regions)

    # if return_dc:
    #    res_data[genompos] = generate_sections(res_data[genompos])

    cursor.close()
    cnx.close()

    return res_data

def get_mane_select_transcript(genomic_locations):
    res_data = {}

    for i, genompos in enumerate(genomic_locations):
        chrom, ref_seq, pos, ref, alt = gencode_adapter.tools.parse_genome_position(genompos)

        if (chrom is not None) and (pos is not None):
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
            cursor = cnx.cursor()

            mysql_q = ("select * from " + conf_reader.__GENCODE_TRANSCRIPTS_TABLE__ + " where " +
                                                                                    "(seqname='chr" + chrom + "') " +
                                                                                    "and (feature='transcript')" +
                                                                                    "and (start_pos <= " + pos + ") " +
                                                                                    "and (end_pos >= " + pos + ");"
                       )

            print(mysql_q)
            cursor.execute(mysql_q)

            res_data[genompos] = []
            result = cursor.fetchall()

            column_names = [desc[0] for desc in cursor.description]
            for ((result)) in enumerate(result):
                json_obj = {}
                for j, feature in enumerate(column_names):
                    json_obj[feature] = result[1][j]

                functional_regions = copy.deepcopy(json_obj)
                res_data[genompos].append(functional_regions)

            #if return_dc:
            #    res_data[genompos] = generate_sections(res_data[genompos])

            cursor.close()
            cnx.close()

    return res_data


def get_functional_element_of_genomic_position_sum(genomic_locations, return_dc=True):
    """
    Gets all functional regions of a genomic location, including gene, transcripts, CDS and UTRs.
    Returns a dictionary with the genomic locations as keys and a list of the found entries in the GENCODE annotation database table

    :param genomic_locations: List of genomic locations, e.g. ['chr7:140753336A>T']
    :param return_dc:
    :return:
    """
    mapping_chrom = {}
    mapping_pos = {}

    q = "SELECT * FROM " + conf_reader.__GENCODE_DATABASE_TABLE__ + " WHERE "
    for genompos in genomic_locations:
        chrom, ref_seq, pos, ref, alt = gencode_adapter.tools.parse_genome_position(genompos)
        if (chrom is not None) and (pos is not None):
            q += "((seqname='chr" + chrom + "') " + "and (start_pos <= " + pos + ") " + "and (end_pos >= " + pos + ")) OR"

            if chrom not in mapping_chrom:
                mapping_chrom[chrom] = []
            mapping_chrom[chrom].append(genompos)

            if pos not in mapping_pos:
                mapping_pos[pos] = []
            mapping_pos[pos].append(genompos)
    q = q.rstrip(" OR ")
    q = q+";"

    cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
    cursor = cnx.cursor()
    #mysql_q = ("select * from " + conf_reader.__GENCODE_DATABASE_TABLE__ + " where " +
    #                                                                            "(seqname='chr" + chrom + "') " +
    #                                                                            "and (start_pos <= " + pos + ") " +
    #                                                                            "and (end_pos >= " + pos + ");"
    #               )
    mysql_q=q
    print(mysql_q)
    cursor.execute(mysql_q)

    res_data = {}
    result = cursor.fetchall()
    features = copy.deepcopy(conf_reader.__FEATURES__)
    for ((result)) in enumerate(result):
        json_obj = {}
        for j, feature in enumerate(features):
            json_obj[feature] = result[1][j]

        seqname = result[1][3]
        start = result[1][6]
        end = result[1][7]
        # Get all genomic locations within the same chromosome
        genompos_chrom = mapping_chrom[seqname]
        # Get all genomic locations within the start and end position
        genompos_pos = []
        for pos in mapping_pos.keys():
            if (pos > int(start)) and (pos < int(end)):
                genompos_pos.append(mapping_pos[pos])

        intersect_genompos_list = [x for x in genompos_chrom if x in genompos_pos]
        for genompos in intersect_genompos_list:
            if genompos not in res_data:
                res_data[genompos] = []
            functional_regions = copy.deepcopy(json_obj)
            res_data[genompos].append(functional_regions)

    #if return_dc:
    #    res_data[genompos] = generate_sections(res_data[genompos])

    cursor.close()
    cnx.close()

    return res_data

def get_functional_element_of_genomic_position(genomic_locations, return_dc=True):
    """
    Gets all functional regions of a genomic location, including gene, transcripts, CDS and UTRs.
    Returns a dictionary with the genomic locations as keys and a list of the found entries in the GENCODE annotation database table

    :param genomic_locations:
    :param return_dc:
    :return:
    """
    res_data = {}

    for i, genompos in enumerate(genomic_locations):
        chrom, ref_seq, pos, ref, alt = gencode_adapter.tools.parse_genome_position(genompos)

        if (chrom is not None) and (pos is not None):
            cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                                  database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
            cursor = cnx.cursor()
            mysql_q = ("select * from " + conf_reader.__GENCODE_DATABASE_TABLE__ + " where " +
                                                                                    "(seqname='chr" + chrom + "') " +
                                                                                    "and (start_pos <= " + pos + ") " +
                                                                                    "and (end_pos >= " + pos + ");"
                       )

            print(mysql_q)
            cursor.execute(mysql_q)
            res_data[genompos] = []
            result = cursor.fetchall()
            features = copy.deepcopy(conf_reader.__FEATURES__)
            for ((result)) in enumerate(result):
                json_obj = {}
                for j, feature in enumerate(features):
                    json_obj[feature] = result[1][j]

                functional_regions = copy.deepcopy(json_obj)
                res_data[genompos].append(functional_regions)

            #if return_dc:
            #    res_data[genompos] = generate_sections(res_data[genompos])

            cursor.close()
            cnx.close()

    return res_data


def gene_position_request(genes, return_dc):
    """
    Retrieves start and end position of genes

    :param genes:
    :return:
    """
    gene_data = {}

    for i, gene in enumerate(genes):
        cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
        cursor = cnx.cursor()

        mysql_q = ("select * from " + conf_reader.__GENCODE_POSITIONS_TABLE__ + " where (gene_name='" + gene + "');")

        print(mysql_q)
        cursor.execute(mysql_q)

        gene_data[gene] = []
        result = cursor.fetchall()

        features = copy.deepcopy(conf_reader.__FEATURES__)
        for ((result)) in enumerate(result):
            json_obj = {}
            for j, feature in enumerate(features):
                json_obj[feature] = result[1][j]
            gene_data[gene].append(copy.deepcopy(json_obj))

        #if return_dc:
        #    gene_data[gene] = generate_sections(gene_data[gene])

        cursor.close()
        cnx.close()

    return gene_data


def gene_request(genes, return_dc):
    """
    Retrieves all functional elements of genes

    :param genes:
    :return:
    """
    res_data={}
    for i, gene in enumerate(genes):
        #elements = gene.split(":")
        #gene = elements[0]
        #variant = elements[1]
        #variant_str = gene + " " + variant

        cnx = pymysql.connect(host=conf_reader.__MODULE_SERVER__, user=user, password=pw,
                              database=conf_reader.__GENCODE_DATABASE__, port=int(conf_reader.__GENCODE_DB_PORT__))
        cursor = cnx.cursor()

        mysql_q = ("select * from " + conf_reader.__GENCODE_DATABASE_TABLE__ + " where (gene_name='" +  gene +  "');")

        print(mysql_q)
        cursor.execute(mysql_q)

        res_data[gene] = []
        result = cursor.fetchall()

        features = copy.deepcopy(conf_reader.__FEATURES__)
        json_obj = {}
        for ((result)) in enumerate(result):
            #print("res ",result[0])
            json_obj={}
            for j, feature in enumerate(features):
                #print("result ",i,":",feature,":",result[0],":",result[1][j])
                json_obj[feature] = result[1][j]
            res_data[gene].append(copy.deepcopy(json_obj))

        #if return_dc:
        #    res_data[gene] = generate_sections(res_data[gene])

        cursor.close()
        cnx.close()

    #if (pos_as_keys is not None):
    #    if pos_as_keys == "genompos":
    #        res_new = {}
    #        for i, var in enumerate(variants):
    #            genompos = genomepos[i]
    #            res_new[genompos] = {}
    #            res_new[genompos]["civic"] = res_data[var]
    #        res_data = res_new

    return res_data

