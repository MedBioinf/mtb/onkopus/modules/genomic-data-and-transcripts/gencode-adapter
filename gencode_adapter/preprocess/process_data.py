from gencode_adapter.db_request import get_protein_sequence, get_refseq_mapping, get_pdb_ids


def get_pdb_mappings(results):
    """
    Get PDB IDs assigned to transcript IDs

    :param results:
    :return:
    """
    enst_ids = []
    enst_id_genompos_dc = {}

    for var in results:
        for transcript_id in results[var]["transcripts"].keys():
            enst_id = transcript_id

            if enst_id not in enst_id_genompos_dc:
                enst_id_genompos_dc[enst_id] = []

            if enst_id not in enst_ids:
                enst_ids.append(enst_id)
                enst_id_genompos_dc[enst_id].append(var)
            else:
                enst_id_genompos_dc[enst_id].append(var)

    pdb_ids = get_pdb_ids(enst_ids)
    for enst_id in pdb_ids:
        if enst_id in enst_id_genompos_dc:
            vars = enst_id_genompos_dc[enst_id]
            if len(pdb_ids[enst_id])>0:
                for var in vars:
                    results[var]["transcripts"][enst_id]["pdb_ids"] = pdb_ids[enst_id]
        else:
            print("Could find no mapping of transcript ID to PDB ID: ",enst_id)

    return results


def get_protein_sequences(results):
    """
    Retrieves amino acid sequences for all transcripts included in a biomarker frame.
    Receives a biomarker frame, where transcripts are stored in the 'transcript_id' feature

    :param results:
    :return:
    """
    enst_ids = []
    enst_id_genompos_dc = {}

    for var in results:
        enst_id = results[var][0]["transcript_id"]
        if enst_id not in enst_id_genompos_dc:
            enst_id_genompos_dc[enst_id] = []

        if enst_id not in enst_ids:
            enst_ids.append(enst_id)
            enst_id_genompos_dc[enst_id].append(var)
        else:
            enst_id_genompos_dc[enst_id].append(var)
    sequences = get_protein_sequence(enst_ids)
    for enst_id in sequences:
        var = enst_id_genompos_dc[enst_id]
        results[var][0]["protein_sequence"] = sequences[enst_id][0]["sequence"]

    refseq_transcripts = get_refseq_mapping(enst_ids)
    print("mappings ",refseq_transcripts)
    for enst_id in refseq_transcripts:
        vars = enst_id_genompos_dc[enst_id]
        for var in vars:
            results[var][0]["refseq_mappings"] = refseq_transcripts[enst_id]

    return results


def get_protein_sequences_full(results):
    """
    Retrieves amino acid sequences for all transcripts included in a biomarker frame.
    Receives a biomarker frame, where transcripts are stored in the 'transcript_id' feature

    :param results:
    :return:
    """
    enst_ids = []
    enst_id_genompos_dc = {}

    for var in results:
        for transcript_id in results[var]["transcripts"].keys():

            enst_id = transcript_id
            if enst_id not in enst_id_genompos_dc:
                enst_id_genompos_dc[enst_id] = []

            if enst_id not in enst_ids:
                enst_ids.append(enst_id)
                enst_id_genompos_dc[enst_id].append(var)
            else:
                enst_id_genompos_dc[enst_id].append(var)

    sequences = get_protein_sequence(enst_ids)
    for enst_id in sequences:
        vars = enst_id_genompos_dc[enst_id]
        if len(sequences[enst_id])>0:
            for var in vars:
                results[var]["transcripts"][enst_id]["protein_sequence"] = sequences[enst_id][0]["sequence"]

    refseq_transcripts = get_refseq_mapping(enst_ids)
    for enst_id in refseq_transcripts:
        if enst_id in enst_id_genompos_dc:
            vars = enst_id_genompos_dc[enst_id]
            for var in vars:
                results[var]["transcripts"][enst_id]["refseq_mappings"] = refseq_transcripts[enst_id]

    return results


def generate_transcript_dictionary(results):
    """
    Transforms the transcripts section of the GENCODE results in a dictionary, with Ensembl transcript IDs as keys

    :param results:
    :return:
    """
    for var in results.keys():
        transcripts = {}
        for transcript in results[var]["transcripts"]:
            if isinstance(transcript, dict):
                transcript_id = transcript["transcript_id"]
                if isinstance(transcript_id,str):
                    if transcript_id not in transcripts:
                        transcripts[transcript_id] = transcript
                    else:
                        print("transcript already identified: ",transcript_id)
                elif isinstance(transcript_id,list):
                    for id in transcript_id:
                        if id not in transcripts:
                            transcripts[id] = transcript
                        else:
                            pass
            else:
                print("No transcript generated: ",transcript)
        results[var]["transcripts"] = transcripts

    return results


def assign_exons_to_transcripts(results):
    """
    Identifies to which transcript a found exon entry belongs and adds the exon entry in the transcript dictionary

    :param results:
    :return:
    """
    results_new = {}
    for var in results.keys():
        results_new[var] = { "gene": results[var]["gene"], "transcripts":results[var]["transcripts"],
                             "transcript_mane_select": {}, "utr": results[var]["utr"], "cds": results[var]["cds"] }
        for exon in results[var]["exon"]:
            transcript_id = exon["transcript_id"]
            if isinstance(results[var]["transcripts"], dict):
                if isinstance(transcript_id, str):
                    if transcript_id in results[var]["transcripts"]:
                        if "exon" in results_new[var]["transcripts"][transcript_id]:
                            results_new[var]["transcripts"][transcript_id]["exon"].append(exon)
                            print("Exon already allocated: ", results_new[var]["transcripts"][transcript_id]["exon"])
                        else:
                            results_new[var]["transcripts"][transcript_id]["exon"] = []
                            results_new[var]["transcripts"][transcript_id]["exon"].append(exon)
                else:
                    print("Exon transcript list: ",transcript_id)
            else:
                print("no transcript dictionary: ",results[var]["transcripts"])
    return results_new


def assign_cds_to_transcripts(results):
    """
    Identifies to which transcript a found CDS entry belongs and adds the CDS entry in the transcript dictionary

    :param results:
    :return:
    """
    results_new = {}
    for var in results.keys():
        results_new[var] = { "gene": results[var]["gene"], "transcripts":results[var]["transcripts"], "transcript_mane_select": {}, "utr": results[var]["utr"] }
        for exon in results[var]["cds"]:
            transcript_id = exon["transcript_id"]
            if isinstance(transcript_id, str):
                if transcript_id in results[var]["transcripts"]:
                    if "cds" in results_new[var]["transcripts"][transcript_id]:
                        results_new[var]["transcripts"][transcript_id]["cds"].append(exon)
                        print("CDS already allocated: ",results_new[var]["transcripts"][transcript_id]["cds"])
                    else:
                        results_new[var]["transcripts"][transcript_id]["cds"] = []
                        results_new[var]["transcripts"][transcript_id]["cds"].append(exon)
            else:
                print("CDS transcript list: ", transcript_id)
    return results_new


def assign_utr_to_transcripts(results):
    """
    Identifies to which transcript a found UTR entry belongs and adds the UTR entry in the transcript dictionary

    :param results:
    :return:
    """
    results_new = {}
    for var in results.keys():
        results_new[var] = { "gene": results[var]["gene"], "transcripts":results[var]["transcripts"], "transcript_mane_select": {} }
        for exon in results[var]["utr"]:
            transcript_id = exon["transcript_id"]
            if isinstance(transcript_id, str):
                if transcript_id in results[var]["transcripts"]:
                    if "utr" in results_new[var]["transcripts"][transcript_id]:
                        results_new[var]["transcripts"][transcript_id]["utr"].append(exon)
                        print("UTR already allocated: ",results_new[var]["transcripts"][transcript_id]["utr"])
                    else:
                        results_new[var]["transcripts"][transcript_id]["utr"] = []
                        results_new[var]["transcripts"][transcript_id]["utr"].append(exon)
            else:
                print("UTR transcript list: ", transcript_id)
    return results_new


def identify_mane_select_transcript(results):
    """
    Identifies the MANE Select transcript and adds it as an additional section

    :param results:
    :return:
    """
    for var in results.keys():
        for transcript in results[var]["transcripts"].keys():
            if "tag" in results[var]["transcripts"][transcript]:
                if isinstance(results[var]["transcripts"][transcript]["tag"],list):
                    if "MANE_Select" in results[var]["transcripts"][transcript]["tag"]:
                        results[var]["MANE_Select_transcript"] = transcript
                elif isinstance(results[var]["transcripts"][transcript]["tag"], str):
                    if results[var]["transcripts"][transcript]["tag"]=="MANE_Select":
                        results[var]["MANE_Select_transcript"] = transcript
    return results
