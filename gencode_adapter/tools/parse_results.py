import traceback




def generate_key_value_pairs_for_attributes_no_sections(results):
    """
    Extracts the features included in the GENCODE 'attributes' section and adds them as key-value pairs to the response

    :param results:
    :return:
    """
    for var in results:
            for result in results[var]:
                if "attribute" in result:
                    try:
                        features = result["attribute"].split(";")
                        for feature in features:
                            elements = feature.split(" ")
                            if len(elements)>2:
                                key = elements[1]
                                val = str(elements[2]).lstrip("\"").rstrip("\"")
                            elif len(elements)==2:
                                key = elements[0]
                                val = str(elements[1]).lstrip("\"").rstrip("\"")
                            else:
                                continue
                            if key not in result:
                                result[key] = str(val)
                            else:
                                if (isinstance(result[key],str)):
                                    var_pre = result[key]
                                    result[key] = []
                                    result[key].append(var_pre)
                                result[key].append(val)
                    except:
                        print("Error extracting attributes: ",traceback.format_exc())
                else:
                    print("Attribute section not found ",result)
    return results

def generate_key_value_pairs_for_attributes(results):
    """
    Extracts the features included in the GENCODE 'attributes' section and adds them as key-value pairs to the response

    :param results:
    :return:
    """
    for var in results:
        for section in results[var]:
            if isinstance(results[var][section],list):
                for result in results[var][section]:
                    if " attribute" in result:
                        try:
                            features = result[" attribute"].split(";")
                            for feature in features:
                                elements = feature.split(" ")
                                if len(elements)>2:
                                    key = elements[1]
                                    val = str(elements[2]).lstrip("\"").rstrip("\"")
                                elif len(elements)==2:
                                    key = elements[0]
                                    val = str(elements[1]).lstrip("\"").rstrip("\"")
                                else:
                                    continue
                                if key not in result:
                                    result[key] = val
                                else:
                                    if (isinstance(result[key],str)):
                                        var_pre = result[key]
                                        result[key] = []
                                        result[key].append(var_pre)
                                    result[key].append(val)
                        except:
                            print("Error extracting attributes: ",traceback.format_exc())
                    else:
                        print("Attribute section not found ",result)
            else:
                print("no list: ",var,": ",section)
    return results


def generate_sections(results):
    """
    Groups retrieved results for a genomic location or gene depending on the functional element. Includes gene, transcript, exon, CDS and UTRs

    :param results:
    :return:
    """
    sections = ["gene", "transcripts", "exon", "cds", "utr", "other"]

    for var in results:
        grouped_results = {}
        for section_type in sections:
            grouped_results[section_type] = []
        for result in results[var]:
                if result[" feature"] == "gene":
                    grouped_results["gene"].append(result)
                elif result[" feature"] == "transcript":
                    grouped_results["transcripts"].append(result)
                elif result[" feature"] == "exon":
                    grouped_results["exon"].append(result)
                elif result[" feature"] == "CDS":
                    grouped_results["cds"].append(result)
                elif result[" feature"] == "UTR":
                    grouped_results["utr"].append(result)
                else:
                    grouped_results["other"].append(result)
        results[var] = grouped_results

    return results
