import unittest, os, io
from app import app


class TestFlaskGeneFunctionalRegions(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_flask_genomic_functional_regions(self):
        q="TP53,CDK7"
        response = self.client.get('/gencode/v1/hg38/GeneFunctionalElements?genes='+q)
        print(response.get_data(as_text=True))