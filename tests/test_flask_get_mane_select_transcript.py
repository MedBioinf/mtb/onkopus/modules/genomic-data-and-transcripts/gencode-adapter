import unittest, os, io
from app import app


class TestFlaskMANESelectTranscript(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_flask_mane_select_transcript(self):
        q="chr1:36471505C>T,chr1:45331485G>T,chr7:140753336A>T"
        response = self.client.get('/gencode/v1/hg38/getMANESelectTranscript?genompos='+q)
        print(response.get_data(as_text=True))