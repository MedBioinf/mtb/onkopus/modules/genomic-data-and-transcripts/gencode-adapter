import unittest
import gencode_adapter.db_request

class TestPDBIDsRequests(unittest.TestCase):

    def test_get_pdbidb_request(self):
        genompos = ["chr7:140753336A>T","chr1:2556718C>T"]
        res = gencode_adapter.db_request.get_mane_select_transcript(genompos)
        print(res)
        enst_ids = ["ENST00000646891.2"]
        res = gencode_adapter.db_request.get_pdb_ids(enst_ids)
        print(res)

