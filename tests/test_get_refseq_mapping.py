import unittest
import gencode_adapter.db_request

class TestRefSeqMappingRequests(unittest.TestCase):

    def test_get_refseq_mapping_request(self):
        genompos = ["chr7:140753336A>T"]
        res = gencode_adapter.db_request.get_mane_select_transcript(genompos)
        print(res)
        enst_ids = [res["chr7:140753336A>T"][0]["transcript_id"]]
        res = gencode_adapter.db_request.get_refseq_mapping(enst_ids)
        print(res)

