import unittest
import gencode_adapter.db_request


class TestProteinSequenceRequests(unittest.TestCase):

    def test_protein_sequence_request(self):
        genompos = ["chr7:140753336A>T"]
        res = gencode_adapter.db_request.get_mane_select_transcript(genompos)
        print(res)
        enst_ids = [res["chr7:140753336A>T"][0]["transcript_id"]]
        res = gencode_adapter.db_request.get_protein_sequence(enst_ids)
        print(res)

