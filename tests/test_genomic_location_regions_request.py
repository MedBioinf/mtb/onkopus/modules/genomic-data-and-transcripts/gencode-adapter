import unittest
import gencode_adapter.db_request


class TestGenomicLocationRegionsRequests(unittest.TestCase):

    def test_genomic_location_regions_request(self):
        genompos = ["chr7:140753336A>T"]
        res = gencode_adapter.db_request.get_functional_element_of_genomic_position(genompos,None)
        print(res)

