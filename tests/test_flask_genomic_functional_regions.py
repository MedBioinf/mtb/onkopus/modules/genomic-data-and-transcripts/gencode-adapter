import unittest, os, io
from app import app


class TestFlaskGenomicFunctionalRegions(unittest.TestCase):

    def setUp(self):
        self.ctx = app.app_context()
        self.ctx.push()
        self.client = app.test_client()

    def tearDown(self):
        self.ctx.pop()

    def test_flask_genomic_functional_regions(self):
        q="chr1:36471505C>T,chr1:45331485G>T"
        response = self.client.get('/gencode/v1/hg38/GenomicFunctionalRegions?genompos='+q)
        print(response.get_data(as_text=True))