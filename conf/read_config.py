import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "MODULE_SERVER" in os.environ:
    __MODULE_SERVER__ = os.getenv("MODULE_SERVER")
else:
    __MODULE_SERVER__ = config['DEFAULT']['MODULE_SERVER']

if "GENCODE_DATABASE" in os.environ:
    __GENCODE_DATABASE__ = os.getenv("GENCODE_DATABASE")
else:
    __GENCODE_DATABASE__ = config['GENCODE']['GENCODE_DATABASE']

if "GENCODE_DATABASE_TABLE" in os.environ:
    __GENCODE_DATABASE_TABLE__ = os.getenv("GENCODE_DATABASE_TABLE")
else:
    __GENCODE_DATABASE_TABLE__ = config['GENCODE']['GENCODE_DATABASE_TABLE']

if "GENCODE_POSITIONS_TABLE" in os.environ:
    __GENCODE_POSITIONS_TABLE__ = os.getenv("GENCODE_POSITIONS_TABLE")
else:
    __GENCODE_POSITIONS_TABLE__ = config['GENCODE']['GENCODE_POSITIONS_TABLE']

if "GENCODE_TRANSCRIPTS_TABLE" in os.environ:
    __GENCODE_TRANSCRIPTS_TABLE__ = os.getenv("GENCODE_TRANSCRIPTS_TABLE")
else:
    __GENCODE_TRANSCRIPTS_TABLE__ = config['GENCODE']['GENCODE_TRANSCRIPTS_TABLE']

if "GENCODE_REFSEQ_MAPPING_TABLE" in os.environ:
    __GENCODE_REFSEQ_MAPPING_TABLE__ = os.getenv("GENCODE_REFSEQ_MAPPING_TABLE")
else:
    __GENCODE_REFSEQ_MAPPING_TABLE__ = config['GENCODE']['GENCODE_REFSEQ_MAPPING_TABLE']

if "GENCODE_PROTEIN_SEQUENCE_TABLE" in os.environ:
    __GENCODE_PROTEIN_SEQUENCE_TABLE__ = os.getenv("GENCODE_PROTEIN_SEQUENCE_TABLE")
else:
    __GENCODE_PROTEIN_SEQUENCE_TABLE__ = config['GENCODE']['GENCODE_PROTEIN_SEQUENCE_TABLE']

if "GENCODE_PDB_ID_TABLE" in os.environ:
    __GENCODE_PDB_ID_TABLE__ = os.getenv("GENCODE_PDB_ID_TABLE")
else:
    __GENCODE_PDB_ID_TABLE__ = config['GENCODE']['GENCODE_PDB_ID_TABLE']

if "GENCODE_DB_PORT" in os.environ:
    __GENCODE_DB_PORT__ = os.getenv("GENCODE_DB_PORT")
else:
    __GENCODE_DB_PORT__ = config['GENCODE']['GENCODE_DB_PORT']

if "FEATURES" in os.environ:
    __FEATURES__ = os.getenv("FEATURES").split(",")
else:
    __FEATURES__ = config['GENCODE']['FEATURES'].split(",")

if "GENCODE_ADAPTER_PORT" in os.environ:
    __GENCODE_ADAPTER_PORT__ = os.getenv("GENCODE_ADAPTER_PORT")
else:
    __GENCODE_ADAPTER_PORT__ = config['GENCODE']['GENCODE_ADAPTER_PORT']

