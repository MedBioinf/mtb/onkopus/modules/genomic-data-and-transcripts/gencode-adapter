FROM python:3.12.0a7-bullseye

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y python3-pip python3-dev

# Create virtual environment
RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip
RUN /opt/venv/bin/pip install mysql-connector-python

WORKDIR /app/
COPY ./requirements.txt /app/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 10132

CMD [ "/opt/venv/bin/python3", "/app/app.py" ]