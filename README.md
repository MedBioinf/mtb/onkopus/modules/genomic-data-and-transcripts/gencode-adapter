# Gencode-Adapter

Genomic data module retrieving genomic functional information from the GENCODE database [1]. 



##### References

Harrow, J., Denoeud, F., Frankish, A., Reymond, A., Chen, C. K., Chrast, J., ... & Guigo, R. (2006). GENCODE: producing a reference annotation for ENCODE. Genome biology, 7(1), 1-9.
